# Johns Cloud Puppet Lab

Here's a documented example of setting up a **secure Puppet engineering environment** which is both useful as-is, and useful as a reference implementation for further work.<br/>

- https://en.wikipedia.org/wiki/Puppet_(software)
- https://www.puppet.com/community/open-source


I refer this setup as a **canonical example** of a technical implementation.

A person with basic **Cloud** and **Linux Bash** skills can replicate this setup within a few hours, and gain understanding of the components while doing so. However, mastering Puppet is a big endeavor.

<br/>

![Johns-Cloud-Puppet-Lab](pix/Johns-Cloud-Puppet-Lab.jpg "Johns-Cloud-Puppet-Lab"){width=100%}

<br/>


## Installation

Read the material below first!

My setup instructions are **annotated text files** of Bash/Linux commands (CLI), in an easy-to-read format, intended for users with Linux skills.<br/>

Although there is much material here, it's written for easy skimming and **reading for gist**.

Most of the work is **copy/pasting** my text commands into your own Bash shells, with modifications appropriate for your own environment and preferences -- eg, user accounts and file paths.

Why not just **automate** the whole process?<br/>

Because the purpose of this demo is **hands-on knowledge transfer**, and showing details of a working system. After you perform these manual steps, you will be in a good position to define your own automations.

### Tip

I deploy **Johns Agile Bashrc** on my MacOS & Linux servers, a large set of Bash conveniences. Most relevant are the `cert-grep` and `ssl_grep` macros (Bash functions) to examine SSL/TLS certificates.<br/>

I recommend this installation on your Linux servers:

<details>
<summary><b>ssl_grep</b></summary>
<pre>
$ mkdir -p ~/zGit/
$ cd       ~/zGit/
$ git clone  https://gitlab.com/umi-ch/bashrc.git  bashrc
$ source   ~/zGit/bashrc/bashrc_101
&nbsp;
$ alias cert_grep cert_grep2 ssl_grep
    alias cert_grep='f_cert_grep'
    alias cert_grep2='f_cert_grep2'
    alias ssl_grep='f_ssl_grep'
&nbsp;
$ ssl_grep https://example.com
&nbsp;    
    0: Certificate:
      Public-Key: (2048 bit)
      Issuer:   C = US, O = DigiCert Inc, CN = DigiCert Global G2 TLS RSA SHA256 2020 CA1
      Subject:  C = US, ST = California, L = Los Angeles, O = Internet\C2\A0Corporation\C2\A0for\C2\A0Assigned\C2\A0Names\C2\A0and\C2\A0Numbers, CN = www.example.org
      Validity:
         Not Before: Jan 30 00:00:00 2024 GMT
         Not After : Mar  1 23:59:59 2025 GMT
      Data:
         Version: 3 (0x2)
         Serial Number: 07:5b:ce:f3:06:89:c8:ad:df:13:e5:1a:f4:af:e1:87
      X509v3 Subject Alternative Name:
         DNS:example.com
         DNS:example.edu
         DNS:example.net
         DNS:example.org
         DNS:www.example.com
         DNS:www.example.edu
         DNS:www.example.net
         DNS:www.example.org
      X509v3 Key Usage:
         Digital Signature
         Key Encipherment
      X509v3 Extended Key Usage:
         TLS Web Client Authentication
         TLS Web Server Authentication
   ...
</pre>
</details>

### Puppet Setup Steps

- ![01.PuppetServer.txt](Steps/01.PuppetServer.txt)
- ![02.UFW.txt](Steps/02.UFW.txt)
- ![03.PostgreSQL.txt](Steps/03.Postgres.txt)
- ![04.PuppetDB.txt](Steps/04.PuppetDB.txt)
- ![05.Agent.txt](Steps/05.Agent.txt)
- ![06.Modules.txt](Steps/06.Modules.txt)
- ![07.PuppetDB-CLI.txt](Steps/07.PuppetDB-CLI.txt)
- ![08.FoxyProxy.txt](Steps/08.FoxyProxy.txt)
- ![09.PuppetBoard.txt](Steps/09.PuppetBoard.txt)
- ![10.Tests.txt](Steps/10.Tests.txt)

### Web app pictures

<details>
    <summary>08_b.FoxyProxy.png</summary>
    <img src='Steps/08_b.FoxyProxy.png'>
    </details>
<details>
    <summary>08_c.Performance-Dashboard.png</summary>
    <br/>
    <pre># - On the Puppet Master:<br/>http://localhost:8085</pre>
    <img src='Steps/08_c.Performance-Dashboard.png'>
    </details>
<details>
    <summary>09_b.PuppetBoard.png</summary>
    <br/>
    <pre># - On the Puppet Master:<br/>http://localhost:8086</pre>
    <img src='Steps/09_b.PuppetBoard.png'>
    </details>

<br/>


## Design goals

Although there are several **"step by step"** guides on the Internet for setting up Puppet, most of them are **incomplete**, piecemeal, out of date, and **not useful for troubleshooting** when errors occur.

- This is mostly due to **unstated assumptions** about the target **ecosystem**, and a "non-example" style of documentation in which setup commands are shown but without feedback as to the results.

- This is often the case with **"black-box automation"** which tries to encapsulate the steps, but leaving you with a technical mess if something goes wrong -- especially if you're not familiar with the underlying technology in the first place.

- Another challenge is that the Puppet developers have released a commercial **enterprise edition**, hence there's less attention paid to maintaining and documenting the free **open-source edition**.

- **Security** considerations are largely unmentioned and unaddressed in those other Internet guides.

My setup steps here **can be automated** by yourself for future work, and the ecosystem shown here is quite **scalable** -- with exceptions noted as they occur.

And although this setup is intended as an **engineering proof-of-concept**, it's adequate for small-scale **production use** as a **working prototype**.

<br/>


## Salient features

You can use your own server setup here, or quickly set up a dedicated **Engineering Ecosystem** aka **Test Environment** aka **Lab** with low-cost / low-effort Cloud computing instances.

- Most of the setup work is done in a **Linux Bash shell** (CLI). On MacOS, I use **Homebrew** installations to supplement the built-in Unix tools.

- **Puppet** is a classic **client/server** system, with a **pull model**, and deployed **agent** software. ( In contrast, **Ansible** uses a client-less **push** model based on SSH commands. ) The Puppet **client** is called the **Puppet agent**, and the Puppet **server** is called **Puppet Master**.

- For convenience and consistent reproducibility, the **Puppet Master** is implemented here with **Docker images** from **Voxpupuli**. The Puppet **Client** has a native installation, so it can make changes to the Linux server.

<br/>


## Requirements


### Controller

- In this demo, I control this Lab ecosystem from my **Mac Laptop**. If you use an MS-Windows laptop, consider installing **WSL** (Windows Subsystem for Linux). A Linux server (or **Linux Desktop**) with a web browser (eg: Firefox) also works well.<br/>

- You need a **DNS service**, and a DNS domain name to which you can add entries. In this demo, mine is: `umi.ch`<br/>

- If you're not already using a Cloud- or corporate- DNS service, I recommend **Zonomi** :<br/>
  https://zonomi.com

- I create a unique DNS name for the controller, for documentation convenience. Mine is: `controller.umi.ch` which is just an A-record pointing to `localhost`.

- I recommend a dedicated SSH public/private key pair for this work, for **separation of concerns**.<br/>
  https://www.redhat.com/sysadmin/manage-multiple-ssh-key-pairs

### Puppet Master

- A dedicated Linux server instance is used to host the **Puppet Master**. Create a unique DNS name. In this demo, mine is: `puppet.umi.ch`<br/>
  
- You need root access, and (preferably) an unprivileged login account with sudo access, eg: `jdb`. This server will run **Docker** to load the Puppet software images.<br/>

- Any **cheap Cloud server** will do. I use a `Digital Ocean Droplet` with 2 Gb RAM.<br/>
  https://www.digitalocean.com/products/droplets<br/>

### Puppet Client

- Any and all Linux servers can have the role a **Puppet Clients** (agents) to be managed by Puppet. Create a unique DNS name for each one. In this demo, mine is: `client.umi.ch`<br/>

- You need root access, and (preferably) an unprivileged login account with sudo access, eg: `jdb`.<br/>

- Any **cheap Cloud server** will do. I use a free-tier `AWS t3.micro` with 1 Gb RAM.<br/>
  https://aws.amazon.com/ec2<br/>
  https://aws.amazon.com/free<br/>

- The **Puppet Master** can also be a **Puppet Client** for self-management, but I don't show that here.<br/>

### FYI

- **Cloud servers** are not needed, these servers can also be on a **private network**. The Puppet servers can also be mere VM's on **single machine** (eg, your Laptop with VMware), but that's less interesting and less useful.

- Any Linux distribution will do. I use **Ubuntu v22.04** aka "Jammy Jellyfish".<br/>
  https://en.wikipedia.org/wiki/Ubuntu

<br/>


## Security

- Multi-cloud is not a problem, but client/server access via Internet connections introduces security risks.

- In this demo, only the **Controller** (Laptop) has SSH private keys. Neither Puppet server makes authenticated output connections, **except** for the **Puppet Client** (agent), which does a "secure pull" from the **Puppet Master**. This connectivity is secured with **Puppet-managed PKI**: SSL/TLS connections and certificates.

- Puppet server software components **Puppet Master**, **PuppetDB**, **PostgreSQL database**, etc communicate with each other without restriction on the Puppet server itself, with no external exposure. If you want to move these components to separate servers, more **security engineering** is needed.

- In this demo, my Linux servers do not contain confidential data nor security credentials
  (keys, passwords) which can access other Internet services.<br/>
  <br/>
  Although this greatly mitigates **server-compromise impact**, there's still a **compromise risk** of servers being **hijacked into a bot-net**, as well as financial risks of **cryptojacking** etc. Thus good IT security is required here... in case that's not obvious in the first place.

- For data privacy, a key goal is **protecting client configurations** on the Puppet Master and Puppet DB, which contain semi-confidential **inventory data** of all Puppet Clients.

- Only **encrypted inbound connections** are allowed to the Puppet servers. In this demo, I allow **inbound SSH** from the Internet to port **2223** with public keys only;<br/>
  password authentication is disallowed. I trust the SSH daemon.<br/>

- However, I'm uncertain how secure is the **Puppet protocol** on port **8140**. Therefore, even though this protocol is protected with **SSL/TLS**, I use **firewall IP restrictions** to restrict connections to the specific Puppet clients. This approach does not scale well past a few clients, but is adequate for this demo.

- In this demo, I use **UFW** : Ubuntu's "Uncomplicated Firewall" which manipulates `Linux iptables`. Beware that that **UFW** and **Docker** need a special tweak to avoid interference which otherwise silently **expose** all "closed" ports inbound connections from anywhere!<br/>
  <br/>
  Be sure to run the "connection refused" validation tests shown in the setup steps.<br/>
  https://www.howtogeek.com/devops/how-to-use-docker-with-a-ufw-firewall/

- For better security and scalability, I recommend **additional firewalling** from Cloud providers, eg:<br/>
  https://docs.aws.amazon.com/vpc/latest/userguide/vpc-security-groups.html<br/>
  https://docs.digitalocean.com/products/networking/firewalls/ 

- FYI: I also use `fail2ban` on my Linux server setups, which is not documented here. If you do this too, be sure to **whitelist** your laptop controller and all instances of these Linux servers, to avoid mysterious lockouts while building and testing this Puppet Lab.

- Two Puppet "web apps" are produced here: **PuppetBoard** and **Performance Dashboard**.  However, the Docker image author (voxpupuli) did not build these with **https** support; only an **unencrypted http service** is provided.<br/>

- In this demo, I also did not engineer an https service for these web apps. Instead, I set up an **SSH tunnel** from my **Mac Laptop** to the **Puppet Master** server<br/> for a secure connection, using the **Foxy Proxy** extension on my **Firefox** browser.<br/>
  <br/>
  This allows safe use of **unencrypted HTTP** connections on an interim basis until full **HTTPS** support is provided -- at the cost of additional **operational complexity**.

- **Certified Security** would require an active monitoring mechanism for this ecosystem. But that's another project topic, and not included here.<br/>

<br/>


## References

Puppet, in general:

- https://www.puppet.com/docs/puppet/7/install_puppet.html
- https://forge.puppet.com/
- https://www.puppet.com/docs/puppetserver/5.3/dev_debugging.html
- https://www.puppet.com/docs/puppetdb/8/overview.html
- https://www.puppet.com/docs/guides/  &nbsp; &nbsp; &nbsp; (a common "broken link" on the Internet)
- https://www.puppet.com/docs/puppet/8/man/epp.html

<br/>

Puppet, via Docker:

- https://www.puppet.com/blog/puppet-docker
- https://hub.docker.com/r/puppet/puppetserver/
- https://hub.docker.com/r/voxpupuli/container-puppetdb
- https://hub.docker.com/r/voxpupuli/puppetboard
- https://hub.docker.com/r/ubuntu/postgres
- https://www.howtogeek.com/devops/how-to-use-docker-with-a-ufw-firewall/

<br/>


## Agile theme
This repository has useful working examples, quickly and iteratively adaptable to new environments.
<br/>


## License
Copyright &copy; 2024, Mountain Informatik GmbH<br>
All rights reserved.<br>
Permission to share is granted under the terms of the **Server Side Public License**<br>
https://www.mongodb.com/licensing/server-side-public-license
<br>

