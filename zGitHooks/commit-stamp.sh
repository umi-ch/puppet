#!/usr/bin/env bash
# Copyright (c) 2019, Mountain Informatik GmbH. All rights reserved.

# - symver.org version
sym_ver='7.4.1'


# File:  ./zGitHooks/commit-stamp.sh

# JDB, 17. May 2019
# - Update the version stamp file. Eg:
#   $ git log main -n 1 --pretty=format:"%ci : %h (%H) : %cn : %S : %s%n" | cat
#      2019-05-17 14:23:17 +0200 : 17089a1 (17089a13cc285b7fabd5058a0bc9c600c3d2dc2f) : John_DB : master : 'updates'
#
# - The latest version stamp is added to the stamp file,
#   and the most recent 10 entries are kept --
#   using sort, with ISO 8601 date/time stamps at the beginning.
#
# - Installation:
#   $ cd repository     # - Eg:  ~/zGit/bashrc/
#   $ touch zStamp
#   $ ( cd .git/hooks/ ; ln -s  ../../zGitHooks/commit-stamp.sh  pre-commit )


file="./zStamp"     # - Time/date-stamp file in the repository directory.


# - $new_commit is a stub of the next commit, with an approximate timestamp.
#   If this script runs as "pre-commit", then the actual commit stamp of the checking
#   is not generated UNTIL AFTER this script completes successfully --
#   but then it's too late to record it here.  ("chicken and egg" problem)
#
# - isodate4 in %ci format, eg: '2019-05-18 09:06:19 +0200'
#
isodate4=$(date "+%Y-%m-%d %H:%M:%S %z")
new_commit="${isodate4} : stub"         # - Stub marker for the the current commit.


# - Determine current branch name. Eg:
#
#   mac$ ( cd ~/zGit/bashrc; git branch | grep '\*' )
#   * master
#
#   mac$ ( cd ~/zGit/ansible; git branch | grep '\*' )
#   * main

branch=$(git branch 2>&1 | grep '\*' | cut -f2 -d' ')
rc=${PIPESTATUS[0]}         # - git branch failed?
if [[ $rc -ne 0 ]]; then
    echo "- git branch fails, rc=$rc"
    echo "${branch}"
    exit $rc
fi


stamp=$(git log ${branch} -n 1 --pretty=format:"%ci : %h (%H) : %cn : %S : '%s'%n"  2>&1)
rc=$?
if [[ $rc -ne 0 ]]; then
    echo "- git log fails, rc=$rc"
    echo "${stamp}"
    exit $rc
fi

out=$(printf "%s\n%s\n%s\n"  "$(egrep -v ' : stub' zStamp | head -10)"  "${stamp}"  "${new_commit}" | sort -r)
rc=$?                       # - sort failed?
if [[ $rc -ne 0 ]]; then
    echo "- printf/sort fails, rc=$rc"
    echo "${out}"
    exit $rc
fi

echo "${out}"  > "${file}"  2>&1
rc=$?
if [[ $rc -ne 0 ]]; then
    echo "- file write failed, rc=$rc"
    echo "${out}"
    exit $rc
fi

out=$(git add "${file}" 2>&1)   # - The updated stamp is also in the commit.
rc=$?
if [[ $rc -ne 0 ]]; then
    echo "- git add fails, rc=$rc"
    echo "${out}"
    exit $rc
fi

exit $rc

