
######################################
# PuppetDB Performance Dashboard     #
# -> View the web app on Mac Firefox #
# -> Enable selected Internet access #
######################################

# - Content appears here after modules are installed and deployed on Puppet Clients (agents).
#
# - For security, I only allow connections from my Mac Laptop.
#
# - FYI:
#   https://stackoverflow.com/questions/51579063/curl-https-via-an-ssh-proxy

# - Because this port is unencrypted, I do not open the UFW firewall;
#       instead, I rely on a simple SSH tunnel and proxy forwarding in Firefox.
#       The SSH private keys used are on the Mac, which has no inbound access.

# - Enable FoxyProxy in Mac Firefox, using local port 9085, to puppet.umi.ch
#       Title:      Ocean PuppetDB
#       Type:       Socks5
#       Hostname:   localhost
#       Port:       9085
#       Proxy DNS:  (enabled)

# - Browse:
#   -> Firefox browse:  url:  http://localhost:8085
#   -> curl browse 1:   mac$  curl -s http://localhost:9085
#   -> curl browse 2:   mac$  curl -s -L --proxy socks5h://0:9085  http://localhost:8085  2>&1 | head -3
#                               <!DOCTYPE html>
#                               <meta charset="utf-8">
#                               <title>PuppetDB: Dashboard</title>


o$ date; whoami; hostname -f
    Fri Feb  9 10:00:00 CET 2024
    jdb
    puppet.umi.ch

o$ sudo ufw status verbose | grep -i 8085
o$ 

o$ cat ~/.ssh/authorized_keys 
    ssh-ed25519 AAAA...3Bp/  Johns-SSH...


# - Confirm: No direct access from the Internet.
#   -> Do this from the MacOS controller laptop

mac$ timeout 10  nc -zv -G 5 puppet.umi.ch 8085; echo rc: $?
    nc: connectx to puppet.umi.ch port 8085 (tcp) failed: Operation timed out
    rc: 1


# - Set up an SSH proxy tunnel.

mac$ ssh -p 2223 -D 9085 -f -C -q -N jdb@puppet.umi.ch
mac$

mac$ pgrep -lf 'ssh .* -D'
    98524 ssh -p 2223 -D 9085 -f -v -N jdb@puppet.umi.ch

mac$ curl -s -L --proxy socks5h://0:9085  http://localhost:8085  2>&1 | head -3
    <!DOCTYPE html>
    <meta charset="utf-8">
    <title>PuppetDB: Dashboard</title>


# - To stop the tunnel:

mac$ kill $(pgrep -f 'ssh .* -D')
mac$ 

mac$ pgrep -lf 'ssh .* -D'
mac$

